// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "CatAndBirdGameMode.h"
#include "CatPlayerController.h"

ACatAndBirdGameMode::ACatAndBirdGameMode()
{
    // use our custom PlayerController class
    PlayerControllerClass = ACatPlayerController::StaticClass();
    
    // set default pawn class to our Blueprinted character
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/BP_CatCharacter"));
    if (PlayerPawnBPClass.Class != NULL)
    {
        DefaultPawnClass = PlayerPawnBPClass.Class;
    }
}
