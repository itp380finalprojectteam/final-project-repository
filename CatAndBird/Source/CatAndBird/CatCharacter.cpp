// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "CatCharacter.h"
#include "Weapon.h"


// Sets default values
ACatCharacter::ACatCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    CatMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CatMesh"));
    RootComponent = CatMesh;
	
	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	
}

// Called when the game starts or when spawned
void ACatCharacter::BeginPlay()
{
	Super::BeginPlay();
    
    // Spawn the weapon, if one was specified
    if (WeaponClass)
    {
        UWorld* World = GetWorld();
        if (World)
        {
            FActorSpawnParameters SpawnParams;
            SpawnParams.Owner = this;
            SpawnParams.Instigator = Instigator;
            
            // Need to set rotation like this because otherwise gun points down
            FRotator Rotation(0.0f, 0.0f, -90.0f);
            
            // Spawn the Weapon
            MyWeapon = World->SpawnActor<AWeapon>(WeaponClass, FVector::ZeroVector,
                                                  Rotation, SpawnParams);
            
            if (MyWeapon)
            {
                // This is attached to "WeaponPoint" which is defined in the skeleton
                MyWeapon->AttachToComponent(GetMesh(),
                                            FAttachmentTransformRules::KeepRelativeTransform);
                MyWeapon->SetMyOwner(this);
            }
        }
    }
    
    if (BackWall)
    {
        MyWeapon->BackWall = BackWall;
    }
    
    // Scoring
    GetWorldTimerManager().SetTimer(ScoreTimer, this, &ACatCharacter::ScoreTick, ScoreUpdateFrequency, true);
}

void ACatCharacter::ScoreTick()
{
    Score += ScoreUpdateInterval;
    BlueprintScoreUpdate();
}

// Laser Effects
void ACatCharacter::OnStartFire()
{
    if (MyWeapon != NULL)
    {
        MyWeapon->OnStartFire();
    }
}

void ACatCharacter::OnStopFire()
{
    if (MyWeapon != NULL)
    {
        MyWeapon->OnStopFire();
    }
}



float ACatCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent,AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        Health -= ActualDamage;
        if (Health <= 0.0f)
        {
            bCanBeDamaged = false;
            DeathProcess();
        }
    }
    return ActualDamage;
}

void ACatCharacter::DeathProcess()
{
    // End the game
    BlueprintDeathProcess();
    GetWorldTimerManager().ClearTimer(ScoreTimer);
    MyWeapon = NULL;
}
