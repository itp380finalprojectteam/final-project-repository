

#pragma once

#include "GameFramework/Character.h"
#include "CatCharacter.generated.h"

UCLASS()
class CATANDBIRD_API ACatCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACatCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    // Laser Effects
    void OnStartFire();

    void OnStopFire();
    
    
    // Health
    float TakeDamage(float Damage, FDamageEvent const& DamageEvent,AController* EventInstigator, AActor* DamageCauser) override;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
    float Health = 1.0f;
    
    void DeathProcess();
    
    UFUNCTION(BlueprintImplementableEvent, Category=Health)
    void BlueprintDeathProcess();
    
    
    // Scoring
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Score)
    float Score = 0.0f;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
    float ScoreUpdateFrequency = 0.5f;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
    float ScoreUpdateInterval = 5.0f;
    
    UFUNCTION(BlueprintImplementableEvent, Category=Score)
    void BlueprintScoreUpdate();

protected:
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
    USkeletalMeshComponent* CatMesh;
    
    UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
    UMeshComponent* CatWeapon;
    
    UPROPERTY(EditAnywhere, Category = Weapon)
    TSubclassOf<class AWeapon> WeaponClass;
    
    UPROPERTY(EditAnywhere, Category = LineTracing)
    AActor* BackWall;
    
    
private:
    class AWeapon* MyWeapon;
    
    FTimerHandle ScoreTimer;
    
    void ScoreTick();
	
};
