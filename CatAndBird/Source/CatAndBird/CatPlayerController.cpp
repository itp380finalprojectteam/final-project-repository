// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "CatPlayerController.h"
#include "CatCharacter.h"


ACatPlayerController::ACatPlayerController()
{
    bShowMouseCursor = true;
    DefaultMouseCursor = EMouseCursor::Crosshairs;
}


void ACatPlayerController::SetupInputComponent()
{
    // set up gameplay key bindings
    Super::SetupInputComponent();
    
    InputComponent->BindAxis("MoveForward", this, &ACatPlayerController::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &ACatPlayerController::MoveRight);
    
    InputComponent->BindAction("Jump", IE_Pressed, this, &ACatPlayerController::Jump);
    
    InputComponent->BindAction("Fire", IE_Pressed, this, &ACatPlayerController::OnStartFire);

    InputComponent->BindAction("Fire", IE_Released, this, &ACatPlayerController::OnStopFire);
}

// Muzzle Flash
void ACatPlayerController::OnStartFire()
{
    Cast<ACatCharacter>(GetPawn())->OnStartFire();
}



void ACatPlayerController::OnStopFire()
{
    Cast<ACatCharacter>(GetPawn())->OnStopFire();
}

void ACatPlayerController::MoveForward(float Value)
{
    if (Value != 0.0f)
    {
        APawn* pawn = GetPawn();
        if (pawn != nullptr)
        {
            pawn->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
        }
    }
}

void ACatPlayerController::MoveRight(float Value)
{
    if (Value != 0.0f)
    {
        APawn* pawn = GetPawn();
        if (pawn != nullptr)
        {
            pawn->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
        }
    }
}

void ACatPlayerController::Jump()
{
    ACatCharacter* pawn = Cast<ACatCharacter>(GetPawn());
    if (pawn != nullptr)
    {
        pawn->Jump();
    }
}
