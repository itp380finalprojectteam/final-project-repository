// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "CatPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CATANDBIRD_API ACatPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
    ACatPlayerController();
    
protected:
    /** True if the controlled character should navigate to the mouse cursor. */
    uint32 bMoveToMouseCursor : 1;
    
    // Begin PlayerController interface
    //virtual void PlayerTick(float DeltaTime) override;
    virtual void SetupInputComponent() override;
    // End PlayerController interface
    
    /** Navigate player to the current mouse cursor location. */
    //void MoveToMouseCursor();
    
    // Movement
    void MoveForward(float Value);
    void MoveRight(float Value);
    
    void Jump();
    
    // Mouse look
    //void UpdateMouseLook();
    
    // Muzzle Flash
    void OnStartFire();

    void OnStopFire();
	
	
};
