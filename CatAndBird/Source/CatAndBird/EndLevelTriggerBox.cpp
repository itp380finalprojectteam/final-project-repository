// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "EnemyCharacter.h"
#include "EndLevelTriggerBox.h"

AEndLevelTriggerBox::AEndLevelTriggerBox()
{
    
}

void AEndLevelTriggerBox::NotifyActorBeginOverlap(class AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);
    
    // When we detect that we are overlapping with another actor we create a new component
    // above us, this is then removed when an actor leaves.
    
    // Other Actor is the actor that triggered the event. Check that is not ourself.
    if (OtherActor && (OtherActor != this))
    {
        AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor);
        if (Enemy)
        {
            Enemy->DeathProcess();
            
            // Reduce our health
            if (CatCharacter != NULL)
            {
                CatCharacter->TakeDamage(0.05f, FDamageEvent(), GetInstigatorController(), this);
            }
        }
    }
}  
