// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/TriggerBox.h"
#include "CatCharacter.h"
#include "EndLevelTriggerBox.generated.h"

/**
 * 
 */
UCLASS()
class CATANDBIRD_API AEndLevelTriggerBox : public ATriggerBox
{
	GENERATED_BODY()
	
public:
    AEndLevelTriggerBox();
    
    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
    
protected:
    UPROPERTY(EditAnywhere)
    ACatCharacter* CatCharacter;
	
};
