// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "EnemyAIController.h"
#include "EnemyCharacter.h"

void AEnemyAIController::BeginPlay() {
	Super::BeginPlay();
	
	
	s = Start;
}

void AEnemyAIController::Tick( float deltaTime )
{
	Super::Tick(deltaTime);
	
	APawn* p = UGameplayStatics::GetPlayerCharacter(this, 0);
	AEnemyCharacter* enemy = Cast<AEnemyCharacter>(GetPawn());

	MoveToActor(p);
	
	enemy->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), -5);
	
	
}

void AEnemyAIController::ChasePlayer() {
	APawn* p = UGameplayStatics::GetPlayerCharacter(this, 0);
	MoveToActor(p);
	
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult &Result)
{
	if(Result.IsSuccess()){
		s = Attack;
	}
}
