// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class CATANDBIRD_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;
	void Tick( float DeltaSeconds ) override;
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult &Result) override;
	
	
private:
	enum State { Start, Chase, Attack, Dead };
	State s;
	float range = 150.0f;
	void ChasePlayer();
	bool AttackEntered = false;
	
};
