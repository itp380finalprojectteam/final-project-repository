// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "Tower.h"


// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	TowerMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TowerMesh"));
	RootComponent = TowerMesh;
	
	spawnTime = 1.0f;
	HP = 10.0f;

}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorldTimerManager().SetTimer(SpawnTimer, this, &ATower::OnSpawnTimer, spawnTime, true);
	
}

// Called every frame
void ATower::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ATower::OnSpawnTimer()
{
    FVector pos = GetActorLocation();
    pos.X -= 20.0f;
    
    const FRotator rot = GetActorRotation();
    AActor* enemy = GetWorld()->SpawnActor<AActor>(EnemyClass, pos, rot);
}

float ATower::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                 AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    
    if (ActualDamage > 0.0f)
    {
        HP = HP - Damage;
        if (HP <= 0)
        {
            DestroyTower();
        }
    }
    return ActualDamage;
}



void ATower::DestroyTower()
{
    Destroy();
}
