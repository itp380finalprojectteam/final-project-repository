// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TowerSpawnManager.h"
#include "Tower.generated.h"

UCLASS()
class CATANDBIRD_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    ATowerSpawnManager * GetMyOwner() { return MyOwner; }
    void SetMyOwner(ATowerSpawnManager* owner) { MyOwner = owner; }
    
    float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                     AController* EventInstigator, AActor* DamageCauser) override;
    void DestroyTower();
	
protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	USkeletalMeshComponent* TowerMesh;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	float HP;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	float spawnTime;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> EnemyClass;
	
	FTimerHandle SpawnTimer;
	
	void OnSpawnTimer();
    
private:
    ATowerSpawnManager* MyOwner;
};
