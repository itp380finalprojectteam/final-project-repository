// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "TowerSpawnManager.h"
#include "Tower.h"
#include "Engine/TargetPoint.h"


// Sets default values
ATowerSpawnManager::ATowerSpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATowerSpawnManager::BeginPlay()
{
	Super::BeginPlay();
	occupiedPoints = new bool[SpawnPoints.Num()];
	for(int i=0; i<SpawnPoints.Num(); i++){
		occupiedPoints[i] = false;
	}
	GetWorldTimerManager().SetTimer(SpawnTimer, this, &ATowerSpawnManager::OnSpawnTimer, spawnTime, true);
}

// Called every frame
void ATowerSpawnManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ATowerSpawnManager::OnSpawnTimer()
{
	if(SpawnPoints.Num() != 0){
		int spawnIndex = FMath::RandRange(0, SpawnPoints.Num()-1);
		if(occupiedPoints[spawnIndex] == false){
			occupiedPoints[spawnIndex] = true;
			const FVector pos = SpawnPoints[spawnIndex]->GetActorLocation();
			const FRotator rot = SpawnPoints[spawnIndex]->GetActorRotation();
			ATower* Tower = GetWorld()->SpawnActor<ATower>(TowerClass, pos, rot);
            Tower->SetMyOwner(this);
		}
		else {
			for(int i=0; i<SpawnPoints.Num(); i++){
				if(occupiedPoints[i] == false){
					OnSpawnTimer();
					return;
				}
			}
			
			for(int i=0; i<SpawnPoints.Num(); i++){
				occupiedPoints[i] = false;
			}
			
			return;
		}
	}
	
}

void ATowerSpawnManager::KillTower(int index)
{
	occupiedPoints[index] = false;
}



