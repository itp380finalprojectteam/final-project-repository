// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "EndLevelTriggerBox.h"
#include "TowerSpawnManager.generated.h"

UCLASS()
class CATANDBIRD_API ATowerSpawnManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerSpawnManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
	TArray<class ATargetPoint*> SpawnPoints;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> TowerClass;
	
	UPROPERTY(EditAnywhere)
	float spawnTime;
	
	FTimerHandle SpawnTimer;
	
	void OnSpawnTimer();
	
	void KillTower(int index);
	
	bool* occupiedPoints;
    
protected:
    UPROPERTY(EditAnywhere)
    ATriggerBox* MyTriggerBox;
	
};
