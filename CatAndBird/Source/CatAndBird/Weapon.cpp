// Fill out your copyright notice in the Description page of Project Settings.

#include "CatAndBird.h"
#include "CatPlayerController.h"
#include "Weapon.h"
#include "Tower.h"
#include "EnemyCharacter.h"

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    
    FireRate = 0.5f;
    WeaponRange = 10000.0f;
	FireTimeout = 5.0f;
	firing = true;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
    
    // Position the weapon relative to the cat
    // Since our mesh isn't rigged
    SetActorRelativeLocation(FVector(0, -3.5, -1.5));
    SetActorRelativeScale3D(FVector(0.6, 0.6, 0.6));
    SetActorEnableCollision(false);
	
}

// Called every frame
void AWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    
    FHitResult Hit;
    GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit);
    
	if (PC) {
		PC->SetBeamSourcePoint(0, GetActorLocation(), 0);

		// Did this hit anything?
		if (Hit.bBlockingHit && (Hit.GetActor() != BackWall))
        {
			PC->SetBeamTargetPoint(0, Hit.Location, 0);
        }
	}
    
    if (Hit.bBlockingHit && (Hit.GetActor() != BackWall))
    {
        // We hit something
        FVector fvector = Hit.Location - GetActorLocation();
        fvector.Normalize();
        FRotator rotator = fvector.Rotation();
        SetActorRotation(rotator);
    }
}

// Laser Effect
void AWeapon::OnStartFire()
{
    WeaponTrace();
	GetWorldTimerManager().SetTimer(WeaponTimer, this, &AWeapon::WeaponTrace, FireRate, true);

}

void AWeapon::OnStopFire()
{
    if (FirePC != nullptr)
    {
        FirePC->DeactivateSystem();
    }
    if (PC != NULL)
    {
        PC->ResetParticles();
    }
	PC = NULL;
	firing = true;

    GetWorldTimerManager().ClearTimer(WeaponTimer);
}

UParticleSystemComponent* AWeapon::CreateWeaponParticle(UParticleSystem* particle, FVector startPos, FVector endPos)
{
    //PC = NULL;
    if (particle)
    {
		if (!PC) {
			PC = UGameplayStatics::SpawnEmitterAttached(particle, GetDefaultAttachComponent());
			GetWorldTimerManager().SetTimer(TimeOutTimer, this, &AWeapon::WeaponTimeout, FireTimeout, false);

		}
        if (PC)
        {
            //PC->SetBeamSourcePoint(0, GetActorLocation(), 0);
            //                PC->SetBeamTargetPoint(0, Hit.ImpactPoint, 0);
            PC->SetBeamSourcePoint(0, startPos, 0);
            PC->SetBeamTargetPoint(0, endPos, 0);
        }
    }
    return PC;
}

void AWeapon::WeaponTimeout() {
    if (PC != NULL)
    {
        PC->ResetParticles();
    }
	firing = false;
	GetWorldTimerManager().ClearTimer(WeaponTimer);
	
}

void AWeapon::WeaponTrace()
{
    if (FirePC != nullptr)
    {
        FirePC->DeactivateSystem();
    }
    
    static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
    
    // Trace to see what is under the mouse cursor
    FHitResult Hit;
    GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit);
    
    // Did this hit anything?
    if (Hit.bBlockingHit && (Hit.GetActor() != BackWall))
    {
        FVector StartPos = GetActorLocation();
        // Calculate end position
        FVector EndPos = Hit.ImpactPoint;
        
        FirePC = CreateWeaponParticle(LaserFX, StartPos, EndPos);
        //UGameplayStatics::SpawnEmitterAtLocation(this, HitEffect, Hit.ImpactPoint);
        

        // Deal damage
        if (Hit.GetActor()->IsA(ATower::StaticClass()))
        {
            ATower* Tower = Cast<ATower>(Hit.GetActor());
            if (Tower)
            {
                Tower->TakeDamage(WeaponDamage, FDamageEvent(), GetInstigatorController(), this);
            }
        }
        else if (Hit.GetActor()->IsA(AEnemyCharacter::StaticClass()))
        {
            AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(Hit.GetActor());
            if (Enemy)
            {
                Enemy->TakeDamage(WeaponDamage, FDamageEvent(), GetInstigatorController(), this);
            }
        }
        
        //AddForce
        FVector ForceLocation = Hit.Location;
        ForceLocation.Z = 20;
        
        URadialForceComponent* ForceComp = NewObject<URadialForceComponent>(this, TEXT("MissileHitForceComp"));
        ForceComp->RegisterComponent();
        ForceComp->SetWorldLocation(ForceLocation);
        ForceComp->SetWorldRotation(Hit.Location.Rotation());
        
        ForceComp->Radius = 300;
        ForceComp->ForceStrength = 100000;
        ForceComp->ImpulseStrength = 100000;
        
        ForceComp->AttachTo(this->GetRootComponent(), NAME_None, EAttachLocation::KeepWorldPosition);
        
        ForceComp->FireImpulse();
        
        ForceComp->DestroyComponent(true);
		
    }
}


