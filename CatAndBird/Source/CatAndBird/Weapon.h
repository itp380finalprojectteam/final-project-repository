// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystemComponent.h"
#include "CatCharacter.h"
#include "Weapon.generated.h"

UCLASS()
class CATANDBIRD_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;


    ACatCharacter * GetMyOwner() { return MyOwner; }
    void SetMyOwner(ACatCharacter* owner) { MyOwner = owner; }
    
    // Muzzle Flash
    void OnStartFire();
    void OnStopFire();
    
    // On hit
    void WeaponTrace();
	void WeaponTimeout();
    
    UParticleSystemComponent* CreateWeaponParticle(UParticleSystem* particle, FVector startPos, FVector endPos);
    
    AActor* BackWall;
    
protected:
    UPROPERTY(EditDefaultsOnly, Category = Effects)
    class UParticleSystem* LaserFX;
    UPROPERTY(Transient)
    class UParticleSystemComponent* FirePC;
    
    UPROPERTY(EditAnywhere)
    float FireRate;
    UPROPERTY(EditAnywhere)
    float WeaponRange;
    
    UPROPERTY(EditAnywhere, Category = Damage)
    float WeaponDamage = 2.0f;
    
    
private:
    ACatCharacter* MyOwner;
	UParticleSystemComponent* PC;
    
    FTimerHandle WeaponTimer;
	FTimerHandle TimeOutTimer;



	bool firing;
	float FireTimeout;
	
};
